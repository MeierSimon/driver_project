from django.test import TestCase
from .test_objects.TestUser import TestUser
from .test_objects.TestTeam import TestTeam
from .test_objects.TestDriver import TestDriver
from django.urls import reverse


class DriverManagerViews(TestCase):
    def setUp(self):
        self.test_user = TestUser()
        self.test_driver = TestDriver()
        self.test_team = TestTeam()

    def login_user(self):
        self.client.login(username=self.test_user.username, password=self.test_user.password)

    def check_template_use_base(self, response):
        self.assertTemplateUsed(response, 'base.html')

    def check_login_parts(self, response):
        self.assertTemplateUsed(response, 'registration/login.html')
        self.check_template_use_base(response)
        self.assertContains(response, 'name="username"')
        self.assertContains(response, 'name="password"')

    def check_logout_parts(self, response):
        self.assertTemplateUsed(response, 'registration/logout.html')
        self.check_template_use_base(response)

    def test_login_view(self):
        response = self.client.get(reverse('login'))
        self.check_login_parts(response)

    def test_list_drivers(self):
        self.login_user()
        response = self.client.get(reverse('drivers'))
        self.check_template_use_base(response)
        self.assertTemplateUsed(response, 'driver_manager/list_drivers.html')
        self.assertContains(response, self.test_driver.first_name)
        self.assertContains(response, self.test_driver.last_name)

    def test_list_teams(self):
        self.login_user()
        response = self.client.get(reverse('teams'))
        self.check_template_use_base(response)
        self.assertTemplateUsed(response, 'driver_manager/list_teams.html')
        self.assertContains(response, self.test_team.team_name)
        self.assertContains(response, self.test_team.team_chef)

    def test_logout_view(self):
        response = self.client.get(reverse('logout'))
        self.check_logout_parts(response)



