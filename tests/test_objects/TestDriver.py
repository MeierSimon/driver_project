from driver_manager .models import Driver


class TestDriver:
    first_name = 'Simon'
    last_name = 'Meier'
    birth_date = '03.03.1996'
    nationality = 'CH'
    image = 'default.png'

    def __init__(self):
        try:
            Driver.objects.get(first_name=self.first_name)
        except Driver.DoesNotExist:
            Driver.objects.create(
                first_name=self.first_name,
                last_name=self.last_name,
                birth_date=self.birth_date,
                nationality=self.nationality,
                image=self.image
            )

    def get_test_driver(self):
        return Driver.objects.get(first_name=self.first_name)
