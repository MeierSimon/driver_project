from driver_manager.models import Team


class TestTeam:
    team_name = 'Simon Racing'
    team_chef = 'Simon Meier'
    chassis = 'Ducati'
    category = 'MotoGP'

    def __init__(self):
        try:
            Team.objects.get(team_name=self.team_name)
        except Team.DoesNotExist:
            Team.objects.create(
                team_name=self.team_name,
                team_chef=self.team_chef,
                chassis=self.chassis,
                category=self.chassis
            )

    def get_test_team(self):
        return Team.objects.get(team_name=self.team_name)
