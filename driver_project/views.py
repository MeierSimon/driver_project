from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib import auth, messages

"""
    View für die Home Seite.
    :return: render home.html    
    """


def home(request):
    return render(request, 'home.html')


"""
    View für Signup.
    :param: request: request vom User.
    :return: render signup.html
    """


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
        else:
            messages.error(request, 'Bitte geben Sie gültige Daten ein.')
            return redirect('signup')
    else:
        form = UserCreationForm()
        return render(request, 'registration/signup.html', {'form': form})


"""
        logout
        :param request: request from user
        :return: render logout.html
        """


def logout(request):
    auth.logout(request)
    return render(request, 'registration/logout.html')
