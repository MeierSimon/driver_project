from django.apps import AppConfig


class DriverManagerConfig(AppConfig):
    name = 'driver_manager'
