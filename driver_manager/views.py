from django.shortcuts import render, redirect
from .models import Team, Driver
from .forms import TeamForm, DriverForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required

"""
    View für die Auflistung der Teams
    """


@login_required()
def list_teams(request):
    teams = Team.objects.all()
    form = TeamForm()
    if request.method == 'POST':
        form = TeamForm(request.POST, request.FILES)
    return render(request, 'driver_manager/list_teams.html', {'teams': teams, 'form': form})


"""
    View für die Aflistung der Fahrer
    """


@login_required()
def list_drivers(request):
    drivers = Driver.objects.all()
    form = DriverForm()
    if request.method == 'POST':
        form = DriverForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, 'Fahrer erfolgreich erstellt.')
    return render(request, 'driver_manager/list_drivers.html', {'drivers': drivers, 'form': form})


"""
    View für das Login
    """


@login_required()
def show_driver(request, driver_id):
    try:
        driver = Driver.objects.get(pk=driver_id)
    except Driver.DoesNotExist:
        messages.error(request, 'Fahrer existiert nicht')
        return redirect(list_drivers)
    form = DriverForm(initial={
        'first_name': driver.first_name,
        'last_name': driver.last_name,
    })
    if request.method == 'POST':
        form = DriverForm(request.POST, request.FILES, instance=driver)
        if form.is_valid():
            driver.first_name = form.cleaned_data['first_name']
            driver.last_name = form.cleaned_data['last_name']
            driver.save()
    return render(request, 'driver_manager/show_driver.html', {'driver': driver})


"""
    View um einzelne Teams anzuzeigen
    """


@login_required()
def show_team(request, team_id):
    try:
        team = Team.objects.get(pk=team_id)
    except Team.DoesNotExist:
        messages.error(request, 'Team existiert nicht')
        return redirect(list_teams)
    form = TeamForm(initial={
        'team_name': team.team_name,
    })
    if request.method == 'POST':
        form = TeamForm(request.POST, request.FILES, instance=team)
        if form.is_valid():
            team.team_name = form.cleaned_data['team_name']
            team.team_chef = form.cleaned_data['team_chef']
            team.save()
    return render(request, 'driver_manager/show_team.html', {'team': team})


"""
    View um einen Fahrer zu löschen
    """


@login_required()
def delete_driver(request, driver_id):
    try:
        driver = Driver.objects.get(pk=driver_id)
        driver.delete()
        messages.success(request, 'Fahrer erfolgreich gelöscht.')
    except Driver.DoesNotExist:
        messages.error(request, 'Es existiert kein Fahrer mit dieser ID')
    return redirect('drivers')
