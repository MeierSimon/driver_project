from django import forms
from .models import Team, Driver


class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = '__all__'


class DriverForm(forms.ModelForm):
    class Meta:
        model = Driver
        fields = '__all__'

