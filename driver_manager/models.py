from django.db import models

"""
    Model für die Teams
    """


class Team(models.Model):
    team_name = models.CharField(max_length=255)
    team_chef = models.CharField(max_length=255)
    chassis = models.CharField(max_length=255)
    category = models.CharField(max_length=255, default="category")

    def __str__(self):
        return '{} {}'.format(self.team_name, self.team_chef)


"""
    Model für die Fahrer    
    """


class Driver(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    birth_date = models.CharField(max_length=255)
    nationality = models.CharField(max_length=255)
    image = models.ImageField(upload_to='drivers', default='/media/drivers/default_picture.jpg')
    team = models.ManyToManyField(Team)

    def __str__(self):
        return '{}'.format(self.first_name)

