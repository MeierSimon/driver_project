from django.contrib import admin
from django.urls import path, include
from . import views

"""
    URL's für den driver_manager
    """

urlpatterns = [
    path('teams', views.list_teams, name='teams'),
    path('drivers', views.list_drivers, name='drivers'),
    path('drivers/<int:driver_id>', views.show_driver, name='show_driver'),
    path('setup/<int:team_id>', views.show_team, name='show_team'),
    path('drvers/<int:driver_id>/delete', views.delete_driver, name='delete_driver'),
]
